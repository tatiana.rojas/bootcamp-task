package com.hexagonal.tasks.infrastructure.entities;

import com.hexagonal.tasks.domain.models.Task;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "task",schema = "public")
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String description;
    @Column(name ="creationdate")
    private LocalDateTime creationDate;
    private boolean completed;

    public TaskEntity(){
    }

    public TaskEntity (Integer id,String title, String description,LocalDateTime creationDate,boolean completed){
        this.id = id;
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
        this.completed = completed;
    }


    public static TaskEntity fromDomainModel(Task task){
        return new TaskEntity(task.getId(),task.getTitle(),task.getDescription(),
                task.getCreationDate(),task.isCompleted());
    }

    public Task toDomainModel(){
        return new Task(id, title, description,creationDate,completed);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
