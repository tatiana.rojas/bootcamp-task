FROM amazoncorretto:17
WORKDIR /app

COPY build/libs/*.jar app/tasks-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD ["java", "-jar", "app/tasks-0.0.1-SNAPSHOT.jar"]



